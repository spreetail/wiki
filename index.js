var express = require('express');
var app = express();
const { json } = require("body-parser");
const axios = require("axios");
app.use(json());

// Endpoint that takes a link to Wikipedia and returns a list of all html links as strings
// that are on the page
// Example page: https://en.wikipedia.org/wiki/1968_San_Francisco_State_Gators_football_team
// Example link: "/wiki/San_Francisco_State_University"
app.get('/links', (req, res) => {

});

// Endpoint that takes a link to Wikipedia and returns a list of all
//  images on the page as strings
// Example page: https://en.wikipedia.org/wiki/Andrew_Blake_(scientist)
// Example image:  "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Andrew_Blake.jpg/220px-Andrew_Blake.jpg"
app.get('/images', (req, res) => {

});

// Endpoint that takes a link to Wikipedia and returns the classification
// using the aylien API with the most used words, this will be an api parameter
// Credentials Docs: https://docs.aylien.com/textapi/#api-keys-and-credentials
// App ID:  e0c4bd7d
// Key: 15459731106b36ce11fd6d78d90dace0
// https://api.aylien.com/api/v1/classify
// Docs: https://docs.aylien.com/textapi/endpoints/#classification
/* 
Sample Response: 
{
    "categories":[
    {
      "label":"economy, business and finance - computing and information technology",
      "code":"04003000",
      "confidence":1
    }
  ]} */
app.get('/classify', (req, res) => {

});

const port = 3000;
app.listen(port, async function () {

    console.log(`listening on port: ${port}`)
});
